Tecnologia em Análise e Desenvolvimento de Sistemas

Setor de Educação Profissional e Tecnológica - SEPT

Universidade Federal do Paraná - UFPR

---

*DS122 - Desenvolvimento de Aplicações Web 1*

Prof. Alexander Robert Kutzke

# Código de exercício para a aula sobre PHP+MySQL

Faça o clone do repositório, corrija o arquivo de credenciais, acesse o arquivo para criação da base de dados e das tabelas, e altere o arquivo `article.php` para que ele aceite a inclusão de novos comentários.

A branch `resolvido` possui uma sugestão de implementação.
